package ca.ionx.labs.losergame.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import ca.ionx.labs.losergame.client.listeners.PacketListener;
import ca.ionx.labs.losergame.server.Packet;

/**
 * A socket wrapper that maintains the server connection, sends
 * packets to the server and alerts a listener of incoming packets.
 * 
 * @author Mesbah Mowlavi
 */
public class SocketManager {
	private final Socket socket;
	private final BufferedReader in;
	private final PrintWriter out;

	private enum SocketState { DISCONNECTED, ESTABLISHED, CONNECTED }
	private SocketState state = SocketState.DISCONNECTED;

	private PacketListener packetListener;

	private Thread listener;

	/**
	 * Creates a new SocketManager that listens to the specified host and port.
	 * @param hostname the address of the server
	 * @param port the port of the server
	 * @throws IOException if an error occurred when created the socket.
	 */
	public SocketManager(String hostname, int port) throws IOException {
		socket = new Socket(hostname, port);
		socket.setKeepAlive(true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		listen();
		state = SocketState.ESTABLISHED;
	}

	/**
	 * Disconnects from the current server, stopping the listener and closing all
	 * in-bound and out-bound streams.
	 * 
	 * @throws IOException if an error occurred when closing the socket.
	 */
	public void disconnect() throws IOException {
		System.out.println("Attempting safe disconnect...");
		sendDisconnect();
		state = SocketState.DISCONNECTED;
		listener.interrupt();
		socket.close();
		in.close();
		out.close();
	}

	private void serverDisconnected() {
		try { socket.close(); } catch (IOException ex) {}
		state = SocketState.DISCONNECTED;
		listener.interrupt();
		packetListener.serverDisconnected();
	}


	private void listen() {
		listener = new Thread(new Runnable() {
			public void run() {
				try {
					//parse requests
					String line;
					while (socket.isConnected()) {
						while (!socket.isClosed() && (line = in.readLine()) != null && state != SocketState.DISCONNECTED) {
							if (line.length() < 1) continue;

							//packet format: <request char>[name][button num] (ex: C15, NJason, Q)
							char packet = line.charAt(0);
							String data = (line.length() > 1 ? line.substring(1) : "");

							handlePacket(packet, data);	
						}
						if (state == SocketState.DISCONNECTED) break;
					}
					System.out.println("Lost connection to server.");
					serverDisconnected();
				} catch (IOException e) {
					System.out.println("Error occurred. " + e.getMessage());
				}
			}
		});
		listener.start();
	}
	private void handlePacket(char packet, String data) {
		//change states as needed
		if (state == SocketState.ESTABLISHED) {
			if (packet == Packet.ACCEPTED) {
				state = SocketState.CONNECTED;
			} else if (packet == Packet.REJECTED) {
				state = SocketState.DISCONNECTED;
			}
		}

		//send packet to listeners
		packetListener.packetReceived(packet, data);
	}

	public void registerPacketListener(PacketListener packetListener) {
		this.packetListener = packetListener; 
	}

	/**
	 * Sends a char-based packet to the server.
	 * @param packet char-based packet. Use the {@link Packet} class
	 * 			for a valid packet characters.
	 */
	public void sendPacket(char packet) { sendPacket(""+packet); }

	/**
	 * Sends a string-based packet to the server.
	 * @param packet string-based packet. Use the {@link Packet} class
	 * 			for a valid packet characters concatenated with the packet data.
	 */
	public void sendPacket(String packet) {
		out.println(packet);
		out.flush();
	}

	/// CONVENIENCE SEND METHODS
	/**
	 * Convenience method. Sends a HANDSAKE packet.
	 * @param username the name of the current player.
	 */
	public void sendHandshake(String username) {
		sendPacket(Packet.HANDSHAKE+username);
	}
	/**
	 * Convenience method. Sends a CLICK packet.
	 * @param key the ID of the button clicked.
	 */
	public void sendClick(int key) {
		sendPacket(Packet.CLICK+""+key);
	}
	/**
	 * Convenience method. Sends a NEXTTURN packet.
	 */
	public void sendNextTurn() {
		sendPacket(Packet.NEXTTURN);
	}
	/**
	 * Convenience method. Sends a LOOSE packet.
	 */
	public void sendLoose() {
		sendPacket(Packet.LOOSE);
	}
	/**
	 *  Convenience method. Sends a LOOSE packet.
	 */
	public void sendDisconnect() {
		sendPacket(Packet.DISCONNECT);
	}

	/**
	 *  Convenience method. Sends a REMATCH packet.
	 */
	public void sendRematch() {
		sendPacket(Packet.REMATCH);
	}
}
