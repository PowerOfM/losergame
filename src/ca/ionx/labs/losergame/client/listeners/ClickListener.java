package ca.ionx.labs.losergame.client.listeners;

import java.awt.event.ActionEvent;

/**
 * An interface to handle button clicks from the GamePanel GUI component.
 * @author Mesbah Mowlavi
 */
public interface ClickListener {
	public void buttonGameClick(ActionEvent e);
	public void buttonNextTurnClick(ActionEvent e);
	public void buttonHelpClick(ActionEvent e);
	public void buttonQuitClick(ActionEvent e);
	
}
