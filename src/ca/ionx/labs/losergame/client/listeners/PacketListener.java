package ca.ionx.labs.losergame.client.listeners;
/**
 * An interface to handle packets received from the SocketManager.
 * @author Mesbah Mowlavi
 */
public interface PacketListener {
	public void packetReceived(char packet, String data);
	
	public void serverDisconnected();
}
