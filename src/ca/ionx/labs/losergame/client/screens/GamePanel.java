package ca.ionx.labs.losergame.client.screens;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ca.ionx.labs.losergame.client.listeners.ClickListener;

public class GamePanel extends JPanel {
	private static final long serialVersionUID = -3947070614426727705L;
	
	public JButton btnTurn, btnQuit, btnHelp;
	public JButton[] gameButtons;
	public JLabel lblStatus;
	
	private ClickListener clickListener;
	
	public GamePanel() {
		this.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		//HEADER
		c.gridy = 0;
		c.gridwidth = 1;
		btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { if (clickListener != null) clickListener.buttonQuitClick(e); }
		});
		c.ipadx = 20;
		c.gridx = 0;
		add(btnQuit, c);
		
		btnHelp = new JButton("?");
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { if (clickListener != null) clickListener.buttonHelpClick(e); }
		});
		c.ipadx = 0;
		c.gridx = 1;
		add(btnHelp, c);
		
		lblStatus = new JLabel("Loading...", SwingConstants.CENTER);
		lblStatus.setPreferredSize(new Dimension(100, 50));
		c.ipadx = 50;
		c.gridx = 2;
		add(lblStatus, c);

		btnTurn = new JButton("Next Turn");
		btnTurn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { if (clickListener != null) clickListener.buttonNextTurnClick(e); }
		});
		c.ipadx = 20;
		c.gridx = 3;
		add(btnTurn, c);
		
		//BUTTONS
		c.gridy = 1;
		gameButtons = new JButton[20];
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.insets = new Insets(10,0,0,0);  //top padding
		c.gridx = 0;
		c.gridwidth = 4;
		add(createGameButtons(), c);
	}
	
	private JPanel createGameButtons() {
		JPanel buttonPanel = new JPanel(new GridLayout(5,4));
				
		for (int i = 0; i < gameButtons.length; i++) {
			gameButtons[i] = new JButton(i == 19 ? "LOSER" : ""+(i+1));
			gameButtons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { if (clickListener != null) clickListener.buttonGameClick(e); }
			});
			buttonPanel.add(gameButtons[i]);
		}
		
		return buttonPanel;
	}
	
	public void registerListener(ClickListener clickListener) {
		this.clickListener = clickListener;
	}
	
	public void setStatus(String msg) {
		lblStatus.setText("<html>" + msg + "</html>");
	}
	
	public void reset() {
		setStatus("Loading...");
		for (JButton btn : gameButtons) {
			btn.setEnabled(true);
			btn.setForeground(Color.BLACK);
		}
	}
	
}
