package ca.ionx.labs.losergame.client.screens;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ca.ionx.labs.losergame.client.ClientEngine;
import ca.ionx.labs.losergame.client.SocketManager;
import ca.ionx.labs.losergame.client.listeners.PacketListener;
import ca.ionx.labs.losergame.server.Packet;

/**
 * The ConnectPanel is a GUI component that displays a welcome message
 * to the user and prompts for a name, server host and port number. It
 * then attempts to establish a connection with the server. If the 
 * handshake is successful, it passes the connection to the engine.
 * 
 * @author Mesbah Mowlavi
 */
public class ConnectPanel extends JPanel {
	private static final long serialVersionUID = -5699553130830609667L;

	private final ClientEngine engine;

	private JLabel lblStatus;
	private JTextField txtHost, txtPort, txtName;
	private JButton btnConnect, btnQuit;

	private String host, username;
	private int port;

	private boolean connected = false;
	private final int TIMEOUT = 10; //s

	List<Thread> workers;

	/**
	 * Creates a new ConnectPanel that displays a welcome message and
	 * prompts the user for a username, server host and port number.
	 * 
	 * @param engine the ClientEngine parent.
	 */
	public ConnectPanel(ClientEngine engine) {
		super();
		this.engine = engine;
		workers = new ArrayList<Thread>();

		//create components
		lblStatus = new JLabel("Welcome to the Loser Game! Enter your name and connect to a server.");

		txtName = new JTextField(20);
		txtHost = new JTextField(20);
		txtHost.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { resetTextFields(); }
		});
		txtPort = new JTextField(20);
		txtPort.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { resetTextFields(); }
		});

		btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { connect(); }
		});
		btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) { quit(); }
		});

		//organize
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		//ROW0
		c.gridy = 0;
		c.gridx = 0;
		c.ipadx = 10;
		c.ipady = 10;
		c.gridwidth = 4;
		add(lblStatus, c);

		//ROW1
		c.gridy = 1;
		c.gridx = 0;
		c.ipadx = 5;
		c.ipady = 5;
		c.gridwidth = 1;
		add(new JLabel("Name:"), c);
		c.gridx = 1;
		c.gridwidth = 3;
		add(txtName, c);

		//ROW2
		c.gridy = 2;
		c.gridx = 0;
		c.gridwidth = 1;
		add(new JLabel("Host:"), c);
		c.gridx = 1;
		c.gridwidth = 3;
		add(txtHost, c);

		//ROW3
		c.gridy = 3;
		c.gridwidth = 1;
		c.gridx = 0;
		add(new JLabel("Port:"), c);
		c.gridx = 1;
		add(txtPort, c);

		c.ipadx = 0;
		c.ipady = 0;
		c.gridx = 2;
		add(btnQuit, c);
		c.gridx = 3;
		add(btnConnect, c);
	}

	private void resetTextFields() {
		txtHost.setForeground(Color.BLACK);
		txtPort.setForeground(Color.BLACK);
	}

	private void connect() {
		//verify data
		if ((host = txtHost.getText()).isEmpty() || (username = txtName.getText()).isEmpty() || txtPort.getText().isEmpty()) { 
			setStatus("All fields must be entered.", Color.RED);
			return;
		}
		try {
			port = Integer.parseInt(txtPort.getText());
		} catch (NumberFormatException ex1) {
			txtPort.setForeground(Color.RED);
			setStatus("Invalid port. Enter a valid number.", Color.RED);
			return;
		}

		//lock form
		setStatus("Attempting to connect...", Color.BLUE);
		enableForm(false);

		//attempt connection on another thread
		new Thread(new Runnable() {
			public void run() {
				workers.add(Thread.currentThread());
				try {
					//create socket
					final SocketManager sm = new SocketManager(host, port);

					//create timeout listener
					final Thread timeout = new Thread(new Runnable() {
						public void run() {
							workers.add(Thread.currentThread());
							try {
								for (int i = 0; i < TIMEOUT; i++) { 
									if (connected || Thread.currentThread().isInterrupted()) return; 
									Thread.sleep(1000);
								}
								sm.disconnect();
								setStatus("Server connection timed out. Game may already have started.", Color.RED);
								enableForm(true);
								stopWorkers();
							} catch (InterruptedException | IOException e) {}
						}
					});

					//send handshake
					sm.registerPacketListener(new PacketListener() {
						@Override
						public void packetReceived(char packet, String data) {
							//listen for acceptance from server
							if (!connected) {
								if (packet == Packet.ACCEPTED) {	
									timeout.interrupt();
									setStatus("Connected! Waiting for opponent...", Color.BLUE);
									connected = true;	
									
								} else if (packet == Packet.REJECTED) {
									timeout.interrupt();
									setStatus("Server rejected the connection.", Color.RED);
									try { sm.disconnect(); } catch (IOException e) {}
									enableForm(true);
									stopWorkers();
									return;

								}
							} else if (connected && packet == Packet.STARTGAME) {
								if (data.length() < 2) {
									setStatus("Invalid server.", Color.RED);
									try { sm.disconnect(); } catch (IOException e) {}
									enableForm(true);
									stopWorkers();
									return;
								}

								setStatus("Opponent connected! Starting game...", Color.GREEN);
								engine.connect(sm, Integer.parseInt(data.substring(0, 1)), username, data.substring(1));
								stopWorkers();

							} else {
								System.out.println("Weird packet recieved: " + packet + data);
							}
						}

						@Override
						public void serverDisconnected() {
							setStatus("Server disconnected.", Color.RED);
							enableForm(true);
							stopWorkers();
							return;							
						}
					});
					sm.sendHandshake(username);

					//start timeout
					timeout.start();

				} catch (IOException ex2) {
					txtHost.setForeground(Color.RED);
					setStatus("Could not connect to the server " + ex2.getMessage() + ".", Color.RED);
					enableForm(true);
					stopWorkers();
					return;
				}
			}
		}).start();
	}
	private void stopWorkers() {
		for (Thread t : workers) {
			t.interrupt();
		}
	}

	private void enableForm(boolean enabled) {
		txtName.setEnabled(enabled);
		txtHost.setEnabled(enabled);
		txtPort.setEnabled(enabled);
		btnConnect.setEnabled(enabled);		
	}

	private void setStatus(String msg, Color color) {
		lblStatus.setText(msg);
		lblStatus.setForeground(color);
	}

	private void quit() {
		engine.shutdown(false);
	}

	public void reset() {
		setStatus("Welcome to the Loser Game! Enter your name and connect to a server.", Color.BLACK);
		enableForm(true);
	}
}
