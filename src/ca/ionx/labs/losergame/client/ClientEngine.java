package ca.ionx.labs.losergame.client;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ca.ionx.labs.losergame.client.listeners.ClickListener;
import ca.ionx.labs.losergame.client.listeners.PacketListener;
import ca.ionx.labs.losergame.client.screens.ConnectPanel;
import ca.ionx.labs.losergame.client.screens.GamePanel;
import ca.ionx.labs.losergame.server.Packet;

/**
 * The Client portion of the Loser Game. It prompts the user for a 
 * server connection using the ConnectPanel, and once a connection
 * has been established, creates and displays a GamePanel. The engine
 * listens to incoming packets and deals with them accordingly.
 * 
 * @author Mesbah Mowlavi
 */
public class ClientEngine extends JFrame implements ClickListener, PacketListener {
	private static final long serialVersionUID = 8680488333921115628L;
	public static final String VERSION = "1.1.13";

	private SocketManager sm = null;

	private enum GameState { CONNECTING, PLAYING, GAMEOVER }
	private GameState state;
	private ConnectPanel con;
	private GamePanel gui;
	private JPanel screens; //CardLayout
	private final String SCREEN_CONNECT = "CONNECT", SCREEN_GAME = "GAME";

	private String playerName, opponentName;
	private int playerNum = 0;
	private int turn = 0;
	private int clicks = 0;

	private final Color SELECTED = Color.BLUE;
	private List<JButton> clickedButtons;

	public ClientEngine() {
		super("Loser Game v" + VERSION);
		clickedButtons = new ArrayList<JButton>();

		setSize(450, 150);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				shutdown(false);
			}
		});

		//create GUIs
		con = new ConnectPanel(this);
		gui = new GamePanel();
		gui.registerListener(this);

		//create screens
		screens = new JPanel(new CardLayout());
		screens.add(con, SCREEN_CONNECT);
		screens.add(gui, SCREEN_GAME);

		add(screens);
		setVisible(true);

		state = GameState.CONNECTING;
	}

	/**
	 * Connects the client to a server using the specified socket manager.
	 * @param sm a SocketManager that is already connected to the server,
	 * 			successfully completed a handshake, and received the
	 * 			STARGAME packet.
	 */
	public void connect(SocketManager sm, int playerNum, String playerName, String opponentName) {
		if (state != GameState.CONNECTING || this.sm != null) throw new IllegalArgumentException("A connection has already been established.");

		//get socket and info
		this.sm = sm;
		this.sm.registerPacketListener(this);
		this.playerNum = playerNum;
		this.playerName = playerName;
		this.opponentName = opponentName;

		//show game screen
		setTitle(getTitle() + " - " + this.playerName);
		((CardLayout) screens.getLayout()).show(screens, SCREEN_GAME);
		pack();

		//play ball!
		state = GameState.PLAYING;
		updateTurnStatus();
	}
	
	private void updateTurnStatus() {
		if (playerNum == turn) 
			gui.setStatus("Your turn.");
		else
			gui.setStatus(opponentName + (opponentName.endsWith("s")?"'":"'s") + " turn..."); //grammar! :D
	}

	private void finishTurn() {
		//finalize buttons
		for (JButton btn : clickedButtons) btn.setEnabled(false);
		clickedButtons.clear();

		//move to next turn
		turn = (turn==0?1:0);
		sm.sendNextTurn();

		//update gui
		updateTurnStatus();
		gui.btnTurn.setEnabled(false);
		clicks = 0;
	}

	private void startNextTurn() {
		//did we loose?		
		if (hasLost()) {
			sm.sendLoose();
			gameOver(false, true);			
		}

		//switch turn & update gui
		turn = (turn==0?1:0);
		updateTurnStatus();
	}

	private boolean hasLost() {
		for (JButton btn : gui.gameButtons)
			if (btn.isEnabled() && !btn.getText().equals("LOSER")) return false;
		return true;
	}

	@Override
	public void packetReceived(char packet, String data) {
		switch (packet) {
		case Packet.CLICK:
			int buttonID = -1;
			try { 
				buttonID = Integer.parseInt(data);
				if (buttonID < 0 || buttonID > 20) throw new NumberFormatException("HAX0R!");
			} catch (NumberFormatException ex) {
				System.out.println("Invalid CLICK packet received! No such button " + data);
				shutdown(true);
				return;
			}

			gui.gameButtons[buttonID].setEnabled(false);
			break;

		case Packet.NEXTTURN:
			startNextTurn();
			break;

		case Packet.LOOSE:
			gameOver(false, false);
			break;

		case Packet.QUIT:
			gameOver(true, false);
			break;

		case Packet.DISCONNECT:
			gameOver(true, false);
			break;

		case Packet.REMATCH:
			if (state == GameState.GAMEOVER) rematch();
			break;
			
		case Packet.REJECTED:
			if (state == GameState.GAMEOVER) {
				JOptionPane.showMessageDialog(this, opponentName + " rejected the rematch.", getTitle(), JOptionPane.INFORMATION_MESSAGE);
				resetGame();
			}
			break;
		}

	}
	@Override
	public void serverDisconnected() {
		JOptionPane.showMessageDialog(this, "Disconnected from server.", "Error", JOptionPane.ERROR_MESSAGE);
		resetGame();		
	}

	private void gameOver(boolean disconnect, boolean thisLoser) { 
		state = GameState.GAMEOVER;

		String msg;
		String title = "Game Over";
		
		if (disconnect) {
			title = "Opponent Disconnected";
			msg = opponentName + " has cowardly disconnected.";
			JOptionPane.showMessageDialog(this, msg, title, JOptionPane.INFORMATION_MESSAGE);
						
		} else {
			if (thisLoser) msg = opponentName + " wins! Better luck next time.";
			else msg = "WINNER! You outsmarted " + opponentName + "!";
			
			//ask for rematch
			msg += "\n\nWould you like a rematch?";
			int response = JOptionPane.showConfirmDialog(this, msg, title, JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (response == JOptionPane.YES_OPTION) {
				sm.sendRematch();
				gui.setStatus("Waiting for opponent to accept rematch request...");
				return;
			}
			
		}
		resetGame();
	}
	
	private void rematch() {
		gui.reset();
		state = GameState.PLAYING;
		turn = 0;
		clicks = 0;
		updateTurnStatus();
	}
	
	private void resetGame() {
		//reset states and variables
		state = GameState.CONNECTING;
		playerNum = 0;
		turn = 0;
		clicks = 0;
		try { sm.disconnect();
		} catch (Exception e) {}
		sm = null;
		
		//reset screens
		gui.reset();
		con.reset();
		setSize(450, 150);
		
		//show connection screen again
		((CardLayout) screens.getLayout()).show(screens, SCREEN_CONNECT);
	}

	public void shutdown(boolean noPrompt) {
		if (!noPrompt && JOptionPane.showConfirmDialog(this, "Are you sure you want to quit?", "Confirm", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) return;
		if (sm != null ) 
			try { sm.disconnect(); } catch (IOException e) {}
		Runtime.getRuntime().exit(0);
	}

	@Override
	public void buttonGameClick(ActionEvent e) {
		if (turn != playerNum) {
			JOptionPane.showMessageDialog(this, "It's not your turn!", this.getTitle(), JOptionPane.WARNING_MESSAGE);
			return;
		}

		if (clicks > 1) {
			JOptionPane.showMessageDialog(this, "You can only pick 1 or 2 buttons!", this.getTitle(), JOptionPane.WARNING_MESSAGE);
			return;
		}

		//valid button click?
		JButton btn = (JButton) e.getSource();		
		if (btn.isEnabled() && btn.getForeground() != SELECTED) {
			clickedButtons.add(btn);
			clicks++;
			btn.setForeground(SELECTED);
			if (btn.getText().equals("LOSER")) {
				sm.sendLoose();
				gameOver(false, true);
			} else {
				sm.sendClick(Integer.parseInt(btn.getText())-1);
				gui.btnTurn.setEnabled(true);
			}
		}
	}

	@Override
	public void buttonNextTurnClick(ActionEvent e) {
		if (turn != playerNum) {
			JOptionPane.showMessageDialog(this, "It's not your turn!", this.getTitle(), JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if (clicks > 0) 
			finishTurn();
		else
			gui.btnTurn.setEnabled(false);
	}

	@Override
	public void buttonQuitClick(ActionEvent e) {
		shutdown(false);
	}

	@Override
	public void buttonHelpClick(ActionEvent e) {
		JOptionPane.showMessageDialog(this, "The Loser game is a turn-based stragey game. Each turn, a player\n" + 
				"may make up to 2 clicks. The player that is forced to click the \n" +
				"LOSE button (or has no buttons left but that one) looses the game.\n" +
				"Turns may not be skipped! \n\n" +
				"Created by Mesbah Mowlavi\n" +
				"Copyright (c) 2014 Ionx Systems.\n" + 
				"Inspiried by work by Jason Saadatmand.",
				"How to Play", JOptionPane.INFORMATION_MESSAGE);
	}


	/**
	 * Entry point of the Client application.
	 * @param args - none
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				new ClientEngine();
			} 
		});
	}
}
