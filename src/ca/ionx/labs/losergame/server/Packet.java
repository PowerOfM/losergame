package ca.ionx.labs.losergame.server;
/**
 * The Packet class contains constant char-based packet data, acting as a
 * protocol between the server and client.
 * <br/><br/>
 * Packet take the form: <b>{@code <request char>[client num][name][button num]}</b>
 * ([] = optional). The optional portions are based on the type of packet:
 * <ul>
 * <li>HANDSHAKE requires <b>{@code name}</b></li>
 * <li>CLICK requires <b>{@code button num}</b></li>
 * <li>STARTGAME requires <b>{@code client num}</b> and <b>{@code name}</b></li>
 * </ul>
 * All others are single-char packets. 
 * 
 * @author Mesbah Mowlavi
 */
public class Packet {
	//client
	public static final char HANDSHAKE = 'H', NEXTTURN = 'T', QUIT = 'Q', CLICK = 'C', LOOSE = 'L', REMATCH = 'M';
	
	//server
	public static final char ACCEPTED = 'A', REJECTED = 'R', STARTGAME = 'S', DISCONNECT = 'X';

}
