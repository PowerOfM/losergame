package ca.ionx.labs.losergame.server;

import java.util.Scanner;

public class ServerConsole implements Runnable {
	
	private Server server;
	
	public ServerConsole(Server server) {
		this.server = server;
	}
	
	@Override
	public void run() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String line;
		while (true) {
			while ((line = sc.nextLine()) != null) {
				String cmd[] = line.split(" ");
				if (cmd.length == 0) continue;

				if (cmd[0].equals("stop")) { 
					System.out.println("Stoping server...");	
					server.stop(); 
					
				} else if (cmd[0].equals("kick")) {
					int client = 0;
					if (cmd.length == 2) {
						try { client = Integer.parseInt(cmd[1]); }
						catch (Exception e) {
							System.out.println("Invalid client ID to kick.");
							continue;
						}
					}
					System.out.println("Kicking client-" + client + "...");							
					server.disconnect(client);
					
				} else if (cmd[0].equals("list")) {
					System.out.println("Current players:");
					for(int i = 0; i < server.playerNames.length; i++)
						System.out.println("-> Client-" + i + ": " + server.playerNames[i]);
				
				} else if (cmd[0].equals("help")) {
					System.out.println("\nValid commands:\n" + 
										"  -> help\t Displays this message.\n" +
										"  -> list\t Lists the names of the current players.\n" + 
										"  -> kick [ID]\t Kicks the specified client from the game.\n" +
										"  -> stop\t Shuts down the server.\n");
				} else {
					System.out.println("Invalid command. Type 'help' for a list of valid commands.");
				}
			}
		}

	}

}
