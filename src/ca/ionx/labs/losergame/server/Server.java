package ca.ionx.labs.losergame.server;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The server component of the LoserGame. The server accepts connections
 * from a maximum of 2 clients, and coordinates packets between them.
 * 
 * @author Mesbah Mowlavi
 */
public class Server {
	public static final int DEFAULT_PORT = 6666;
	public static final String VERSION = "1.1.16";

	private ServerSocket server;
	private Thread console;
	
	private enum ServerState { WAITING, CONNECTING, PLAYING, GAMEOVER, DISCONNECTED }
	private ServerState state;
	private static final int P1 = 0, P2= 1;

	private Thread[] listeners = new Thread[2];
	private Socket[] sockets = new Socket[2];
	public String[] playerNames = new String[2];
	private PrintWriter[] outputStreams = new PrintWriter[2];

	private boolean[] ready = new boolean[]{false, false};
	private boolean[] rematch = new boolean[]{false, false};


	/**
	 * Constructs a server object that listens to the specified port.
	 * @param port listening port.
	 * @throws IOException
	 */
	public Server(int port) throws IOException {
		System.out.println("     LOSER GAME SERVER v" + VERSION);
		System.out.println("===================================");
		server = new ServerSocket(port);
		System.out.println("Started server on port " + port);
		
		//start console
		console = new Thread(new ServerConsole(this));
		console.start();
		
		//start server
		acceptIncoming();
	}

	/**
	 * Listens for and accepts incoming connections.
	 * @throws IOException
	 */
	private void acceptIncoming() throws IOException {
		state = ServerState.WAITING;
		System.out.println("Waiting for clients to connect...");

		//max 2 clients
		int i = 0;
		while (i < 2) {
			//handle one connection at a time
			final int client = i;
			final Socket socket = server.accept();
			socket.setKeepAlive(true);

			Thread handler = new Thread(new Runnable() {
				public void run() {
					try {
						listeners[client] = Thread.currentThread();
						sockets[client] = socket;
						listen(client);
					} catch (IOException ex) {
						System.out.println("Error at client-"+client+". "+ex.getMessage());
					} finally {
						disconnect(client);
					}
				}
			});

			handler.start();
			i++;
		}
	}

	/**
	 * Listens for packets from a client connection.
	 * 
	 * @param cient the client ID number
	 * @throws IOException
	 */
	private void listen(int client) throws IOException {
		Socket socket = sockets[client];
		if (socket == null) throw new IOException("Socket must not be null!");

		state = ServerState.CONNECTING;
		System.out.println("> Client-" + client + " connected");

		//buffer streams
		BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		outputStreams[client] = out;

		//parse requests
		String line;
		while (socket.isConnected()) {
			while (!socket.isClosed() && (line = in.readLine()) != null && state != ServerState.DISCONNECTED) {
				if (line.length() < 1) continue;

				//packet format: <request char>[name][button num] (ex: C15, NJason, Q)
				char packet = line.charAt(0);
				String data = (line.length() > 1 ? line.substring(1) : "");

				process(packet, data, client);
			}
			if (state == ServerState.DISCONNECTED) break;
		}
		System.out.println("> Lost connection to client-"+client+"!");
	}

	/**
	 * Processes packets based on the current status of the game after receiving requests.
	 * 
	 * @param packet the request char
	 * @param data information associated with the packet
	 * @param client client number (either 0 or 1)
	 */
	private synchronized void process(char packet, String data, int client) {
		switch (state) {
		case CONNECTING:
			if (packet != Packet.HANDSHAKE)
				sendPacket(client, Packet.REJECTED);

			ready[client] = true; 
			playerNames[client] = data;
			System.out.println("Client-"+client+"> Handshake.");

			sendPacket(client, Packet.ACCEPTED);
			System.out.println("> Client-" + client + " ready.");

			//check if both clients are ready
			if (ready[P1] && ready[P2]) {
				sendPacket(P1, Packet.STARTGAME+""+P1+playerNames[P2]);
				sendPacket(P2, Packet.STARTGAME+""+P2+playerNames[P1]);
				System.out.println("> Game started.");
				state = ServerState.PLAYING;
			}
			break;


		case PLAYING:
			if (packet == Packet.CLICK) {
				System.out.println("Client-" + client + "> Clicked button " + data);
				sendPacket(other(client), packet+data);

			} else if (packet == Packet.NEXTTURN) {
				System.out.println("Client-" + client + "> Next turn");
				sendPacket(other(client), packet+data);

			} else if (packet == Packet.LOOSE) {
				System.out.println("Client-" + client + "> Clicked LOSER button");
				sendPacket(other(client), packet+data);
				System.out.println("> Game over.");
				state = ServerState.GAMEOVER;

			} else if (packet == Packet.QUIT || packet == Packet.DISCONNECT) {
				System.out.println("Client-" + client + "> " + (packet == Packet.DISCONNECT?"Disconnected":"Quit"));
				disconnect(client);

			} else {
				System.out.println("> Unknown packet recieved from client-"+client+": "+packet+data);			
			}
			break;


		case GAMEOVER:
			if (packet == Packet.REMATCH) {
				System.out.println("Client-" + client + "> Rematch");
				rematch[client] = true;

				//check if both clients want a rematch
				if (rematch[P1] && rematch[P2]) {
					sendPacket(P1, Packet.REMATCH);
					sendPacket(P2, Packet.REMATCH);
					System.out.println("> New match started!");
					state = ServerState.PLAYING;					
				}

			} else if (packet == Packet.QUIT || packet == Packet.DISCONNECT) {
				System.out.println("Client-" + client + "> " + (packet == Packet.DISCONNECT?"Disconnected":"Quit"));
				if (rematch[other(client)]) sendPacket(other(client), Packet.REJECTED);
				disconnect(client);

			} else {
				System.out.println("> Unknown packet recieved from client-"+client+": "+packet+data);					
			}
			break;


			//invalid states to recieve packets
		case WAITING:
		case DISCONNECTED:
			break;
		}

	}

	/**
	 * Disconnects all clients (blaming the disconnect on the specified client) and restarts the server.
	 * @param client the client number that caused the disconnect.
	 */
	public void disconnect(int client) {
		if (state == ServerState.DISCONNECTED) return;

		state = ServerState.DISCONNECTED;
		System.out.println("Closing all connections...");

		//close connection
		if (sockets[client] != null)
			try { 
				sockets[client].close(); 
			} catch (Exception e) {}

		//notify other
		if (sockets[other(client)] != null)
			try {
				sendPacket(other(client), Packet.DISCONNECT);
				sockets[other(client)].close();
			} catch (Exception e) {}

		//shutdown listeners
		for (Thread t : listeners) {
			if (t==null) continue;
			t.interrupt();
			try {
				t.join();
			} catch (Exception e) {}
		}

		//reset server vars
		sockets = new Socket[2];
		playerNames = new String[2];
		outputStreams = new PrintWriter[2];
		ready = new boolean[]{false, false};
		rematch = new boolean[]{false, false};
		System.out.println("Server restarted.");

		try {
			acceptIncoming(); 
		} catch (IOException e) {
			System.out.println("Server critical error: " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Closes all connections and shuts down the server. 
	 */
	public void stop() {
		state = ServerState.DISCONNECTED;
		System.out.println("Closing all connections...");

		//close connection
		for (Socket socket : sockets) 
			try { socket.close(); } catch (Exception e) {}

		//shutdown listeners
		for (Thread t : listeners) {
			if (t==null) continue;
			t.interrupt();
			try {
				t.join();
			} catch (Exception e) {}
		}
		console.interrupt();
		
		System.out.println("Server stopped. Goodbye!");
		System.exit(0);
	}


	/**
	 * Laziness function: returns the other client.
	 */
	private int other(int x) { return x==0?1:0; }

	/**
	 * Sends a packet to the client.
	 * @param client client number
	 * @param packet data
	 */
	private void sendPacket(int client, char packet) {
		sendPacket(client, ""+packet);
	}
	/**
	 * Sends a packet to the client.
	 * @param client client number
	 * @param packet data
	 */
	private void sendPacket(int client, String packet) {
		PrintWriter out = outputStreams[client];
		if (out != null) {
			out.println(packet);
			out.flush();
		}
	}

	public static void main(String[] args) throws IOException {
		try {
			if (args.length == 1)
				new Server(Integer.parseInt(args[0]));
			else 
				new Server(DEFAULT_PORT);
		} catch (IOException e) {
			System.out.println("Server critical error: " + e.getMessage());			
		} catch (NumberFormatException ex) {
			System.out.println("Argument for PORT NUMBER is invalid.");
		}
	}
}